
import pandas as pd
df = pd.read_csv('logs.csv',index_col=False)

def ligne(df):
    data = {'id': [], 'initial_text': [], "corrected_text": []}
    for i in df.index:
        if df["initial_text"].iloc[i] != df["corrected_text"].iloc[i]:
            data["id"].append(df["id"].iloc[i])
            data["initial_text"].append(df["initial_text"].iloc[i])
            data["corrected_text"].append(df["corrected_text"].iloc[i])

    return pd.DataFrame(data)


def verif(df):

    data = {'mistake': [], 'correction': [], "input_id": [],"id":[]}
    for i in df.index:
        text1 = df["initial_text"].iloc[i].replace("\n", "").replace("( ", "(").replace(" )", ")").replace(".", ". ")
        text2 = df["corrected_text"].iloc[i].replace("\n", "").replace("( ", "(").replace(" )", ")").replace(".", ". ")
        if i == 64:
            print(text1)
            print(text2)
        text1 = text1.split(" ")
        text2 = text2.split(" ")


        aa = 0
        bb = 0
        for j in range(1,len(text1)):
                                        #  a text 1  -- > a  b  c  d  e  f g
                                        #  b text 1  ---> a1 b2 c3 d4 e5 f6
            tab = [None,None,None,None] #
            if aa < len(text1):
                tab[0] = text1[aa]
                if aa+1 < len(text1):
                    tab[1] = text1[aa+1]
                aa = aa + 1
            if bb < len(text2):
                tab[2] = text2[bb]
                if bb+1 < len(text2):
                    tab[3] = text2[bb+1]
                bb = bb + 1
                                            #[f,f,f6,None]


            if tab[0] != tab[2] : #[None,None,None,None]

                if str(tab[0]).upper() == str(tab[3]).upper():    # [a,b,b,a]
                    data["mistake"].append(str(tab[0]))
                    data["correction"].append(str(tab[2])+" "+str(tab[3]))
                    bb = bb + 1
                elif str(tab[1]).upper() == str(tab[2]).upper():
                    data["mistake"].append(str(tab[0])+" "+str(tab[1]))
                    data["correction"].append(str(tab[2]))
                    aa = aa + 1
                else:
                    data["mistake"].append(str(tab[0]))
                    data["correction"].append(str(tab[2]))

                data["input_id"].append(i)
                data["id"].append(df["id"].iloc[i])

    return pd.DataFrame(data)


if __name__ == '__main__':
    mistakes = verif(ligne(df))
    print(mistakes.head(20))
    mistakes.to_csv("mistakes.csv", index=False)