# text1 = 'exposes a problem with free speech and online media. Today, with the expand of "technological sphere", anyone can publish anything and based on the free speech, the regulation seem impossible. This problem has been shown in Donald Trump case against Twitter. In fact, president Trump has been banned from the Twitter\'s platform because he published "fake news" about the presidential election and this fakes news created a mob of this supporter who stormed the Capitol. In order to stop that, Twitter\'s banned Donald Trump for encouragement of violence. This case exposes a normative conflict: free speech against freedom of contract. Indeed, Twitter\'s term and service stat that the platform can delete any profile at any time without a justification or appeal processe'
# text2 = ' a problem with free speech and online media. Today, with the expand of "technological sphere", anyone can publish anything and based on the free speech, the regulation seem impossible. This problem has been show in Donald Trump case angainst Twitter. In fact, president Trump has been banned from the Twitter\'s plateform because he published "fake news" about presidential election and this fakes news created a mob of this supporter who stormed the Capitol. \nIn order to stop that, Twitter\'s banned Donald Trump for encouragement of violence. This case expose a normative conflict : free speech against freedom of contrat. Indeed, Twitter\'s term and service stat that the plateform can delete any profil at any time without an justification or appel process'
#
# a = text1.replace("\n", "").split(" ")
# print(len(text1.split(" ")))
#
# b = text2.replace("\n", "").split(" ")
# cc = len(text2.split(" "))
import pandas as pd
df = pd.read_csv('logs.csv',index_col=False)

def ligne(df):
    data = {'id': [], 'initial_text': [], "corrected_text": []}
    for i in df.index:
        if df["initial_text"].iloc[i] != df["corrected_text"].iloc[i]:
            data["id"].append(df["id"].iloc[i])
            data["initial_text"].append(df["initial_text"].iloc[i])
            data["corrected_text"].append(df["corrected_text"].iloc[i])

    return pd.DataFrame(data)




def verif(df):

    data = {'mistake': [], 'correction': [], "input_id": [],"id":[]}
    for i in df.index:
        text1 = df["initial_text"].iloc[i].replace("\n", "").split(" ")
        text2 = df["corrected_text"].iloc[i].replace("\n", "").split(" ")
        aa = 0
        bb = 0
        for j in range(1,len(text1)):
            tab = [None,None,None,None]
            if aa < len(text1):
                tab[0] = text1[aa]
                if aa+1 < len(text1):
                    tab[1] = text1[aa+1]
                aa = aa + 1
            if bb < len(text2):
                tab[2] = text2[bb]
                if bb+1 < len(text2):
                    tab[3] = text2[bb+1]
                bb = bb + 1

            if tab[0] != tab[2] : #[None,None,None,None]
                r = False
                if len(str(tab[0])) < len(str(tab[3])):
                    r = str(tab[0]).upper() in str(tab[3]).upper()
                else:
                    r = str(tab[3]).upper() in str(tab[0]).upper()

                # if tab[0] == tab[3]:
                if r == True:
                    data["mistake"].append(str(tab[0]))
                    data["correction"].append(str(tab[2])+" "+str(tab[3]))
                    bb = bb + 1
                elif tab[1] == tab[2]:
                    data["mistake"].append(str(tab[0])+" "+str(tab[1]))
                    data["correction"].append(str(tab[2]))
                    aa = aa + 1
                else:
                    data["mistake"].append(str(tab[0]))
                    data["correction"].append(str(tab[2]))

                data["input_id"].append(i)
                data["id"].append(df["id"].iloc[i])

    return pd.DataFrame(data)


if __name__ == '__main__':
    mistakes = verif(ligne(df))
    mistakes.to_csv("mistakes.csv", index=False)